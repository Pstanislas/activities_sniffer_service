from __future__ import print_function

import logging
import grpc

#sys.path.append('./generated')
from generated import activities_sniffer_service_pb2_grpc as as_service_grpc
from generated import activities_sniffer_service_pb2 as as_service

def getActivities(stub):
    print("test0")
    responses = stub.GetActivities(as_service.GetActivities_Request(page_address="test.com"))
    print("test1")
    print(responses)

def run():
    # NOTE(gRPC Python Team): .close() is possible on a channel and should be
    # used in circumstances in which the with statement does not fit the needs
    # of the code.
    with grpc.insecure_channel('localhost:50051') as channel:
        stub = as_service_grpc.ActivitiesSniffer_ServiceStub(channel)
        print("-------------- GetFeature --------------")
        getActivities(stub)


if __name__ == '__main__':
    logging.basicConfig()
    run()
