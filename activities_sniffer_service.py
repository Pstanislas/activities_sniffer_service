from concurrent import futures

import grpc
import logging

import sys

from generated import activities_sniffer_service_pb2_grpc as as_service_grpc
from generated import activities_sniffer_service_pb2 as as_service

import dataTest


def get_activities(link):
    print(dataTest.activity_example)
    print(link)

    return dataTest.activity_example


class ActivitiesSniffer_ServiceServicer(as_service_grpc.ActivitiesSniffer_ServiceServicer):

    def __init__(self):
        self.dataset = "test"

    def GetActivities(self, request, context):
        return as_service.GetActivities_Response(Activities=[dataTest.activity_example,dataTest.activity_example])


def serve():
    server = grpc.server(futures.ThreadPoolExecutor(max_workers=10))
    as_service_grpc.add_ActivitiesSniffer_ServiceServicer_to_server(
        ActivitiesSniffer_ServiceServicer(), server)
    server.add_insecure_port('[::]:50051')
    server.start()
    server.wait_for_termination()


if __name__ == '__main__':
    logging.basicConfig()
    serve()
