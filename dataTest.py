import sys
sys.path.append('./generated')
import activities_sniffer_service_pb2

# Activity example definition
activity_example = activities_sniffer_service_pb2.Activity()
activity_example.ID = "AAAAAA69"
activity_example.Title = "Lac du Montagnon"
# activity_example.DateInfo = date_example
activity_example.Place = "Pyrenees"
# activity_example.DifficultyInfo = date_example
# activity_example.RegistrationInfo = registration_example
activity_example.Responsible = "Stanislas Pedebearn"
# activity_example.Description = description_example
activity_example.Link = "http://google.fr"

# Date example definition
date_example = activity_example.DateInfo
date_example.beginning = "21/03/1996";
date_example.end = "21/03/1996";

# RegistrationInfo example definition
registration_example = activity_example.RegistrationInfo
registration_example.State = "En cour"
registration_example.NbMax = 10
registration_example.NbOk = 4
registration_example.NbWaitingList = 3

# DifficultyInfo example definition
difficulty_example = activity_example.DifficultyInfo
difficulty_example.Physical = 2
difficulty_example.Technical = 3
difficulty_example.Elevation = 1680

# DescriptionInfo example definition
description_example = activity_example.DescriptionInfo
description_example.General = "Petite sortie bien pepeaire."
description_example.Level = "Le niveau sera soutenu, attention ..."
description_example.Equipment = "Pas de materiel particuier. Seulement baudrier et casque."